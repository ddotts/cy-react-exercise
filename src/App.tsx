import { createBrowserRouter, Link, RouterProvider } from 'react-router-dom';
import './App.css';
import { ExerciseOne } from './pages/exercise-one';
import { ExerciseTwo } from './pages/exercise-two/exercise-two';
import { PropsWithChildren } from 'react';

const NavWrapper = ({ children }: PropsWithChildren) => {
  return (
    <>
      <div className={'header'}>
        <Link to={'/'}>Homepage</Link>
        <Link to={'/exercise-one'}>Exercise 1</Link>
        <Link to={'/exercise-two'}>Exercise 2</Link>
      </div>
      {children}
    </>
  );
};

const HomePage = () => {
  return (
    <div>
      <h2>Homepage</h2>
    </div>
  );
};

const router = createBrowserRouter([
  {
    path: '/exercise-one',
    element: (
      <NavWrapper>
        <ExerciseOne />
      </NavWrapper>
    ),
  },
  {
    path: '/exercise-two',
    element: (
      <NavWrapper>
        <ExerciseTwo />
      </NavWrapper>
    ),
  },
  {
    path: '/',
    element: (
      <NavWrapper>
        <HomePage />
      </NavWrapper>
    ),
  },
]);

function App() {
  return (
    <>
      <RouterProvider router={router} />
    </>
  );
}

export default App;
