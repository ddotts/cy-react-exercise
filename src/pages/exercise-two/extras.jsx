import { cartContentUrl } from './exercise.jsx';

const cartEntryDetailsUrl = (id) =>
  `https://660eae82356b87a55c4fc034.mockapi.io/cyr/Cart/${id}`;

export const getItemDetails = (id) =>
  fetch(cartEntryDetailsUrl(id)).then((response) => response.json());

// examples for add and remove from cart
const addEntry = (item) =>
  fetch(cartContentUrl, {
    method: 'POST',
    body: JSON.stringify(item),
  })
    .then((response) => response.json())
    .then((json) => console.log(json));

const removeEntry = (id) =>
  fetch(cartEntryDetailsUrl(id), {
    method: 'DELETE',
  })
    .then((response) => response.json())
    .then((json) => console.log(json));
