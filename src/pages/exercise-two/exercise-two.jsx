import React, { useState, useEffect, useCallback } from 'react';
import { SampleCode } from './sample-code';
import { ExcerciseList, cartContentUrl, shopItemListUrl } from './exercise';

// fetch example
const getCartContents = () =>
  fetch(cartContentUrl).then((response) => response.json());

const logShopContents = () =>
  fetch(shopItemListUrl)
    .then((response) => response.json())
    .then((json) => console.log(json));

const CartItem = ({ entryId, itemId, ammount, remove }) => {
  const onClick = useCallback(() => remove(entryId), [entryId, remove]);
  return (
    <li>
      itemId: {itemId}, ammount: {ammount}{' '}
      <button onClick={onClick}> Remove </button>{' '}
    </li>
  );
};

const useCartContents = () => {
  const [cart, setCart] = useState([]);
  useEffect(() => {
    getCartContents().then((cartContents) => {
      setCart(cartContents);
    });
  }, []);

  return cart;
};

const Cart = () => {
  const cart = useCartContents();
  const remove = useCallback((entryId) => {
    console.log('TODO', entryId);
  }, []);
  return (
    <>
      <h2>Cart Contents </h2>
      <ul>
        {cart.map((item) => (
          <CartItem
            key={item.id}
            entryId={item.id}
            itemId={item.itemId}
            ammount={item.ammount}
            remove={remove}
          />
        ))}
      </ul>
    </>
  );
};

export const ExerciseTwo = () => {
  return (
    <>
      <ExcerciseList />
      <hr />
      <Cart />
      <hr />
      <button onClick={() => logShopContents()}>
        Log contents of the shop
      </button>
      <SampleCode />
    </>
  );
};
