import React, { useReducer, useState, useEffect, useMemo } from 'react';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { thunk } from 'redux-thunk';

// https://react-redux.js.org/api/hooks
import { Provider, useSelector, useDispatch } from 'react-redux';
// redux examples
const defaultState = {
  countExample: 0,
};

const sampleReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'example-count': {
      return {
        ...state,
        countExample: state.countExample + action.payload,
      };
    }
    default:
      return state;
  }
};

const store = createStore(
  combineReducers({ sampleReducer }),
  applyMiddleware(thunk),
);

const ReduxExamples = () => {
  // redux hook examples
  const reduxDispatch = useDispatch();
  const reduxExample = useSelector((state) => state.sampleReducer);

  return (
    <>
      <h4>Redux Count: {reduxExample.countExample}</h4>
      <button
        onClick={() => reduxDispatch({ type: 'example-count', payload: 1 })}
      >
        Add
      </button>
    </>
  );
};

const ReactReducerExamples = () => {
  // react hooks examples
  const [reactExample, reactDispatch] = useReducer(sampleReducer, defaultState);

  return (
    <>
      {' '}
      <h4>React Count: {reactExample.countExample}</h4>
      <button
        onClick={() => reactDispatch({ type: 'example-count', payload: 1 })}
      >
        Add
      </button>
    </>
  );
};

export const SampleCode = () => {
  const [showSamples, setShowSamples] = useState(false);
  return (
    <Provider store={store}>
      <button onClick={() => setShowSamples(!showSamples)}>
        Toggle sample code
      </button>
      {showSamples && (
        <>
          <h2>Sample Code</h2>
          <h3>Reducer examples</h3>
          <ReduxExamples />
          <ReactReducerExamples />
        </>
      )}
    </Provider>
  );
};
