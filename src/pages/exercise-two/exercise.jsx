export const cartContentUrl =
  'https://660eae82356b87a55c4fc034.mockapi.io/cyr/Cart';
export const shopItemListUrl =
  'https://660eae82356b87a55c4fc034.mockapi.io/cyr/CartItem';

export const ExcerciseList = () => (
  <>
    <h2>Hello!</h2>
    <h3>
      We have a shopping cart with items in it and a shop with available items
    </h3>
    <h4>- Display the name and count of items in the cart </h4>
    <h4>- Allow adding and removing items from the cart</h4>
    <h4>- Display a list with all the items in the shop</h4>
    <p>
      The initial cart is found in <b>cartContentUrl</b>, and the list of
      available items in the shop in <b>shopItemListUrl</b>.
    </p>
  </>
);
