const data = {
  cart: [
    {
      id: 1,
      name: 'T-shirt',
      amount: 1,
      itemId: 1,
      totalPrice: 11.5,
    },
    {
      id: 2,
      name: 'Soda',
      amount: 2,
      itemId: 2,
      totalPrice: 5,
    },
    {
      id: 3,
      name: 'Trousers',
      amount: 1,
      itemId: 3,
      totalPrice: 30.5,
    },
    {
      id: 4,
      name: 'Soda',
      amount: 1,
      itemId: 2,
      totalPrice: 2.5,
    },
  ],
  customer: 'Pepe Silvia',
};

export const ExerciseOne = () => {
  return (
    <div>
      <h2>Exercise one</h2>
      <h3>Hello!</h3>
      <h4>1) Render the cart items: name and price</h4>
      <h4>2) Sort the List: price ascending</h4>
      <h4>3) Sum up the price for the shopping cart</h4>
      <h4>4) Merge duplicates (amount, totalPrice)</h4>
    </div>
  );
};
