import { useEffect, useMemo } from 'react';

type Brand = {
  name: string;
  id: string;
  products: CartItem[];
};

type CartItem = {
  brand: string;
  createdAt: string;
  id: string;
  name: string;
  price: number;
};

const getProducts = (() => {}) as unknown as (
  brandId: string,
) => Promise<CartItem[]>;

export const useGetBrandProducts = (brand: Brand) => {
  useEffect(() => {
    getProducts(brand.id).then((products) => {
      brand.products = products;
    });
  }, [brand]);
};

export const useBrandProductsFormatted = (brand: Brand) => {
  return useMemo(
    () => brand.products.map((product) => `${product.name}: ${product.price}`),
    [brand],
  );
};
